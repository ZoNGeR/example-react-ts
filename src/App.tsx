import React from 'react';
import logo from './logo.svg';
import './App.css';
import Parent from './components/Parent';
import Child from './components/Child';
import Portal from './components/Portal';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Example "How work event bubbling in React"
        </p>
        <Parent>
          <Child>
            <Portal>
              <h1>Portal</h1>
            </Portal>
          </Child>
        </Parent>
      </header>
    </div>
  );
}

export default App;
