import React, { useEffect } from "react";

const Parent = ({ children }: { children: React.ReactNode }) => {

  useEffect(() => {
    document.getElementById('parent')?.addEventListener('click', onClickDOM);
    document?.addEventListener('click', onClickDOC);
    return () => {
      document.getElementById('parent')?.removeEventListener('click', onClickDOM);
      document?.removeEventListener('click', onClickDOC);
    }
  }, []);

  const onClick = (e: { preventDefault: () => void; }) => {
    e.preventDefault();
    console.log('parent', { action: 'onClick' });
  }

  const onClickDOM = (e: { preventDefault: () => void, stopPropagation: () => void; }) => {
    e.preventDefault();
    console.log('parent', { action: 'onClickDOM' });
  }

  const onClickDOC = (e: { preventDefault: () => void, stopPropagation: () => void; }) => {
    e.preventDefault();
    console.log('DOCUMENT', { action: 'onClickDOC' });
  }

  return (
    <div id="parent" onClick={onClick} style={{ background: '#f06', padding: 30 }}>
      {children}
    </div>
  );
}

export default Parent;