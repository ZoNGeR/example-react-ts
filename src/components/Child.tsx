import React, { useEffect } from "react";

const Child = ({ children }: { children: React.ReactNode }) => {

  useEffect(() => {
    document.getElementById('child')?.addEventListener('click', onClickDOM);
    return () => {
      document.getElementById('child')?.removeEventListener('click', onClickDOM);
    }
  }, []);

  const onClick = (e: { preventDefault: () => void, stopPropagation: () => void; }) => {
    e.preventDefault();
    console.log('Child', { action: 'onClick', e });
  }

  const onClickDOM = (e: { preventDefault: () => void, stopPropagation: () => void; }) => {
    e.preventDefault();
    console.log('Child', { action: 'onClickDOM', e });
  }

  return (
    <div id="child" onClick={onClick} style={{ background: '#0f6', padding: 30 }}>
      {children}
    </div>
  );
}

export default Child;