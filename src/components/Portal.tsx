import React, { useEffect } from "react";
import ReactDOM from "react-dom";

const container = document.createElement('section');
const Portal = ({ children }: { children: React.ReactNode }) => {

  useEffect(() => {
    container.classList.add('portal')
    document.body.appendChild(container)
    document.getElementById('portal')?.addEventListener('click', onClickDOM);
    return () => {
      document.getElementById('portal')?.removeEventListener('click', onClickDOM);
      document.body.removeChild(container)
    }
  }, [])


  const onClick = (e: { preventDefault: () => void, stopPropagation: () => void; }) => {
    e.preventDefault();
    console.log('portal', { action: 'onClick' });
  }

  const onClickDOM = (e: { preventDefault: () => void, stopPropagation: () => void; }) => {
    e.preventDefault();
    console.log('portal', { action: 'onClickDOM' });
  }

  return ReactDOM.createPortal((
    <div id="portal" onClick={onClick} style={{ background: '#06f', padding: 30 }}>
      {children}
    </div>
  ), container);
}

export default Portal;